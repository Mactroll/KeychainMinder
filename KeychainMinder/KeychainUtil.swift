//
//  KeychainUtil.swift
//  KeychainMinder
//
//  Created by Joel Rennich on 7/10/17.
//  Copyright © 2017 Joel Rennich. All rights reserved.
//

import Foundation
import Security
import OpenDirectory

// set up for callbacks

public protocol KeychainMinderDelegate: class {
    func operationComplete(success: Bool, message: String)
}

let keychainUtil = KeychainUtil()

class KeychainUtil {
    
    var defaultKeychain: SecKeychain? = nil
    var myErr: OSStatus? = nil
    
    weak public var delegate: KeychainMinderDelegate?
    
    init() {
        
    }
    
    // check for locked keychain
    
    func checkLock() -> Bool {
        
        var myKeychainStatus = SecKeychainStatus()
        
        // get the default keychain
        
        myErr = SecKeychainCopyDefault(&defaultKeychain)
        
        if myErr == OSStatus(errSecSuccess) {
            
            myErr = SecKeychainGetStatus(defaultKeychain, &myKeychainStatus)
            
            if Int(myKeychainStatus) == 2 {
                print("Keychain is locked")
                return true
            }
            print("Keychain is unlocked")
            return false
        } else {
            print("Error checking to see if the Keychain is locked, assuming it is.")
            return true
        }
    
    }
    
    // change keychain password
    
    func changeKeychainPassword(oldPass: String, newPass: String) {
        
        // first we check the old password
        
        if !checkOld(oldPass: oldPass) {
            print("Old password incorrect.")
            self.delegate?.operationComplete(success: false, message: "Old password incorrect.")
            return
        }
        
        // next check the current password
        
        if !checkConsolePassword(pass: newPass) {
            print("Current password incorrect.")
            self.delegate?.operationComplete(success: false, message: "Current password incorrect.")
            return
        }
        
        // if both old and new are good we continue
        
        let kerbUtil = KerbUtil()
        
        let success = kerbUtil.changeKeychainPassword(oldPass, newPass)

        if success == 1 {
            print("Successfully changed keychain password.")
            self.delegate?.operationComplete(success: true, message: "Keychain password successfully changed.")
        } else {
            print("Error changing keychain password")
            self.delegate?.operationComplete(success: false, message: "Error changing keychain password.")
        }
    }
    
    // create a new keychain
    
    func createNewKeychain(newPass: String) {
        
        let kerbUtil = KerbUtil()
        
        myErr = kerbUtil.resetKeychain(newPass)
        
        if myErr == OSStatus(errSecSuccess) {
            print("Successfully reset keychain.")
            self.delegate?.operationComplete(success: true, message: "Keychain successfully reset.")
        } else {
            print("Error resetting keychain: " + (myErr?.description)!)
            self.delegate?.operationComplete(success: false, message: "Error resetting keychain.")
        }
        
    }
    
    // utility functions
    
    // get current console user
    
    private func getCurrentConsoleUserRecord() -> ODRecord? {
        
        let currentConsoleUserName: String = NSUserName()
        let uid: String = String(getuid())
        
        // Get ODRecords where record name is equal to the Current Console User's username
        let session = ODSession.default()
        var records = [ODRecord]()
        do {
            let node = try ODNode.init(session: session, type: UInt32(kODNodeTypeLocalNodes))
            let query = try ODQuery.init(node: node, forRecordTypes: kODRecordTypeUsers, attribute: kODAttributeTypeRecordName, matchType: UInt32(kODMatchEqualTo), queryValues: currentConsoleUserName, returnAttributes: kODAttributeTypeNativeOnly, maximumResults: 0)
            records = try query.resultsAllowingPartial(false) as! [ODRecord]
        } catch {
            print("Unable to get local user account OD Records")
        }
        
        // We may have gotten multiple ODRecords that match username,
        // So make sure it also matches the UID.
        if ( records != nil ) {
            for case let record in records {
                let attribute = "dsAttrTypeStandard:UniqueID"
                if let odUid = try? String(describing: record.values(forAttribute: attribute)[0]) {
                    if ( odUid == uid) {
                        return record
                    }
                }
            }
        }
        return nil
    }
    
    // check current console user password
    
    private func checkConsolePassword(pass: String) -> Bool {
        
        let currentUser = getCurrentConsoleUserRecord()
        
        // bail if we don't get a user record
        
        if currentUser == nil {
            return false
        }
        
        // check password
        
        do {
            try currentUser?.verifyPassword(pass)
        } catch {
            return false
        }
        
        // password is good
        
        return true
    }
    
    // check old password
    
    private func checkOld(oldPass: String) -> Bool {
        
        myErr = SecKeychainCopyDefault(&defaultKeychain)
        
        if myErr != OSStatus(errSecSuccess) {
            return false
        }
        
        // silly dance to handle in/out variabled
        
        var oldPassTemp = oldPass
        
        // attempt to unlock the keychain
        
        myErr = SecKeychainUnlock(defaultKeychain, UInt32(oldPass.characters.count), oldPassTemp, true)
        
        if myErr == OSStatus(errSecSuccess) {
            return true
        } else {
            return false
        }
    }
}
