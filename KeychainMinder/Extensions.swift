//
//  Extensions.swift
//  KeychainMinder
//
//  Created by Joel Rennich on 7/11/17.
//  Copyright © 2017 Joel Rennich. All rights reserved.
//

import Foundation
import Cocoa

extension NSWindow {
    func forceToFrontAndFocus(_ sender: AnyObject?) {
        NSApp.activate(ignoringOtherApps: true)
        self.makeKeyAndOrderFront(sender);
    }
}
