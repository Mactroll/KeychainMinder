//
//  Preferences.swift
//  KeychainMinder
//
//  Created by Joel Rennich on 7/11/17.
//  Copyright © 2017 Joel Rennich. All rights reserved.
//

import Foundation

/// A convenience name for `UserDefaults.standard`

let defaults = UserDefaults.standard

/// The preference keys for the Keychain Minder defaults domain.

/// Use these keys, rather than raw strings.

enum Preferences {
    
    static let windowText = "WindowText"
}
