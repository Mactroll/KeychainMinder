//
//  ViewController.swift
//  KeychainMinder
//
//  Created by Joel Rennich on 7/11/17.
//  Copyright © 2017 Joel Rennich. All rights reserved.
//

import Cocoa

class ViewController: NSViewController, KeychainMinderDelegate {
    
    @IBOutlet weak var oldPass: NSSecureTextField!
    @IBOutlet weak var newPass: NSSecureTextField!
    
    @IBOutlet weak var changeButton: NSButton!
    @IBOutlet weak var resetButton: NSButton!
    @IBOutlet weak var spinner: NSProgressIndicator!
    
    override func viewWillAppear() {
        
        if keychainUtil.checkLock() {
            
            print("Keychain is locked, doing our thing.")
            
        } else {
            
            print("Keychain isn't locked, no need for us.")
            
            NSApp.terminate(nil)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }
    
    @IBAction func clickChange(_ sender: Any) {
        
        startOps()
        keychainUtil.delegate = self
        keychainUtil.changeKeychainPassword(oldPass: oldPass.stringValue, newPass: newPass.stringValue)
        
    }

    @IBAction func clickReset(_ sender: Any) {
        
        startOps()
        keychainUtil.delegate = self
        keychainUtil.createNewKeychain(newPass: newPass.stringValue)
        
    }
    
    // callback functions
    
    func operationComplete(success: Bool, message: String) {
        
        print("Operation done.")
        stopOps()
        if !success {
            
            print("Unable to complete operation: " + message)
            // show an alert
            let alert = NSAlert()
            alert.messageText = message
            alert.runModal()
        } else {
            print("Operation complete: " + message)
            NSApp.terminate(nil)
        }
        
    }
    
    // utility functions
    
    func startOps() {
        spinner.startAnimation(nil)
        changeButton.isEnabled = false
        resetButton.isEnabled = false
    }
    
    func stopOps() {
        spinner.stopAnimation(nil)
        changeButton.isEnabled = true
        resetButton.isEnabled = true
    }

}

